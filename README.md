# Book Reader

<!-- [<img src="https://f-droid.org/badge/get-it-on.png" -->
<!--       alt="Get it on F-Droid" -->
<!--       height="80">](https://f-droid.org/app/com.gitlab.matsievskiysv.documentviewer) -->

Document Viewer is a highly customizable document viewer for Android.

Supports the following formats:
* PDF
* DjVu
* EPUB
* XPS (OpenXPS)
* CBZ (Comic Books, no support for rar compressed CBR)
* FictionBook (fb2)

Collaboration with electronic publication sites and access to online ebook catalogs is allowed by the supported OPDS protocol.

FAQ, information about supported MIME types, and available Intents may be found in the original [Wiki](https://github.com/SufficientlySecure/book-reader/wiki).

## [Changelog](https://gitlab.com/matsievskiysv/book-reader/-/blob/master/book-reader/src/main/assets/about/en/changelog.md)

## Development

Book Reader is a fork of a [fork](https://github.com/SufficientlySecure/book-reader/) of the last GPL version of EBookDroid (http://code.google.com/p/ebookdroid/).

We need your support to fix outstanding bugs, join development by forking the project!

## Building

### Build with Gradle

1. Set path to Android SDK `echo 'sdk.dir = /path/to/sdk' > local.properties`
1. Pull git submodules `git submodule update --init --recursive`
1. Execute `./gradlew build`

### NDK Debugging

1. ``cd book-reader; ndk-build -j8 NDK_DEBUG=1``
1. From Android Studio: Run -> Debug... to build and install the APK and launch it on the device.
1. ``cp src/main/AndroidManifest.xml . # Hack required for ndk-gdb to find everything``
1. ``ndk-gdb``

### Development with Android Studio

1. Clone the project from github
2. From Android Studio: File -> Import Project -> Select the cloned top folder
3. Import project from external model -> choose Gradle

# Licenses
Document Viewer is licensed under the GPLv3+.
The file LICENSE includes the full license text.

## Details
Document Viewer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Document Viewer is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Document Viewer.  If not, see <http://www.gnu.org/licenses/>.

## Java Libraries
* JCIFS
  http://jcifs.samba.org/
  LGPL v2.1

* Color Picker by Daniel Nilsson
  http://code.google.com/p/color-picker-view/
  Apache License v2

## C Libraries

* MuPDF - a lightweight PDF, EPUB, CBZ and XPS viewer
  http://www.mupdf.com/
  AGPLv3+

* djvu - a lightweight DJVU viewer based on DjVuLibre
  http://djvu.sourceforge.net/
  GPLv2

## Images

* application_icon.svg
  http://rrze-icon-set.berlios.de/
  Creative Commons Attribution Share-Alike licence 3.0
