package org.ebookdroid.ui.settings.fragments;

import com.gitlab.matsievskiysv.bookreader.R;

import android.annotation.TargetApi;

@TargetApi(11)
public class BackupFragment extends BasePreferenceFragment {

    public BackupFragment() {
        super(R.xml.fragment_backup);
    }
}
